<?php

namespace App\Http\Controllers;

use App\Models\Gejala;
use Illuminate\Http\Request;

class GejalaController extends Controller
{

    public function index()
    {
        $results = Gejala::all();

        $response['status'] = 'success';
        $response['results'] = $results;
        $response['message'] = 'Data ditemukan';

        return response()->json($response, 200);
    }

}
