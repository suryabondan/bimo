<?php

namespace App\Http\Controllers;

use App\Models\Aturan;
use App\Models\Penyakit;
use Illuminate\Http\Request;

class KalkulasiController extends Controller
{
    public function index(Request $request)
    {
        $datas = $request->input('result');

        $response['status'] = false;
        $response['results'] = [];
        $response['message'] = 'result not found';

        if (count($datas) > 0) {

            $penyakits = [];

            foreach ($datas as $key => $data) {
                $aturans = Aturan::select('kode_penyakit')->where('kode_gejala', $data['kode'])->get();


                foreach ($aturans as $penyakit) {
                    if (!isset($penyakits[$penyakit->kode_penyakit][$data['kode']])) {
                        $penyakits[$penyakit->kode_penyakit][$data['kode']] = [];
                    }


                    $penyakits[$penyakit->kode_penyakit][$data['kode']] = $data['cf_value'];
                }

            }


            $response['status'] = true;
            $response['results'] = [];
            $response['message'] = 'result found';


            foreach ($penyakits as $key => $tes) {
                $kode_penyakit = $key;
                $value_min = min($tes);

                $get_cf_pakar = Penyakit::where('kode',$kode_penyakit)->first();

                $result = array(
                    'kode_penyakit' => $get_cf_pakar->kode,
                    'nama_penyakit' => $get_cf_pakar->nama,
                    'persentase_cf' => round($value_min * $get_cf_pakar->cf_pakar, 2),
                );

                array_push($response['results'], $result);

            }
        }

        return response()->json($response);


    }
}
