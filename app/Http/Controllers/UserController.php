<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        $email = $request->input('email');
        $password = $request->input('password');

        $user = User::where('email', $email)->first();
        if (!$user) {
            return response()->json(['message' => 'Login failed'], 401);
        }


        $generateToken = bin2hex(random_bytes(40));
        $user->update([
            'token' => $generateToken
        ]);

        $response['status'] = true;
        $response['results'] = $user;
        $response['message'] = "Login berhasil";

        return response()->json($response);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8'
        ]);

        $save = User::create([
            'nama' => $request->nama,
            'email' => $request->email,
            'password' => Hash::make($request->email),
        ]);

        if ($save) {
            $response['status'] = true;
            $response['message'] = "User berhasil disimpan";
            $response['code'] = 200;
        }else {
            $response['status'] = false;
            $response['message'] = "Data gagal disimpan";
            $response['code'] = 200;
        }

        return response()->json($response, $response['code']);
    }
}
