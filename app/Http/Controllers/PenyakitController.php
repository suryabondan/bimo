<?php

namespace App\Http\Controllers;

use App\Models\Penyakit;
use Illuminate\Http\Request;

class PenyakitController extends Controller
{
    public function index()
    {
        $results = Penyakit::all();

        $response['status'] = 'success';
        $response['results'] = $results;
        $response['message'] = 'Data ditemukan';

        return response()->json($response, 200);
    }


}
