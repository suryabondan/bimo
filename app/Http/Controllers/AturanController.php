<?php

namespace App\Http\Controllers;

use App\Models\Aturan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AturanController extends Controller
{
    public function index()
    {
        $results = DB::select('select a.id, a.kode_gejala, g.nama as nama_gejala, a.kode_penyakit , p.nama as nama_penyakit, a.cf_pakar from aturan a left join gejala g on a.kode_gejala = g.kode left join penyakit p on a.kode_penyakit = p.kode');

        $response['status'] = 'success';
        $response['results'] = $results;
        $response['message'] = 'Data ditemukan';

        return response()->json($response, 200);
    }

}
